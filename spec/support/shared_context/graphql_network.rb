RSpec.shared_context 'with stubbed graphql network context' do
  let(:graphql_network) do
    instance_double(Gitlab::Triage::GraphqlNetwork, query: graphql_response)
  end
end
